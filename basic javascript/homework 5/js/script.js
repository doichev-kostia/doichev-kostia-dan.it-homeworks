// Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
function createUser(name,surname,userBirthday){
    return{
        firstName: name,
        lastName: surname,
        birthday: userBirthday,
        getAge: function (){
            const dateArray = this.birthday.split('/');//birthday = 09/12/2011
            console.log(dateArray);//[0]: "09" [1]: "12" [2]: "2011"
            const today = new Date();
            const birthday = new Date(dateArray[2],dateArray[1] - 1,dateArray[0]);
            today.setHours(0,0,0,0);// today.set time 0h,0m,0s
            let age = (today - birthday)/ 31556952000;//today ms - birthday ms / year ms(something like(365 * 24 * 60 * 60 * 1000))
            console.log(age)// 9.128181961299685
            return age.toFixed(0);//9
        },
        getLogin: function (){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getPassword: function (){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        }
    }

}

const userName = prompt("Enter your name","");
const userLastName = prompt("Enter yor last name","");
const userBirthday = prompt("Enter your date of birthday","DD/MM/YYYY");
const newUser = createUser(userName, userLastName,userBirthday);//userName,userLastName

console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getLogin());
console.log(newUser.getPassword());













// let age = today.getFullYear() - birthday.getFullYear();
//
// if ((today.getMonth() - birthday.getMonth()) <= 0){
//     age = age - 1
// }else {
// }
// if ((today.getDate() - birthday.getDate()) <= 0){
// }else {
//     age = age + 1
// }
// console.log(age)
//




