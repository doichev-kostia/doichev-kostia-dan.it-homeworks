const changeThemeBtn = document.querySelector(".change-theme");

function changeTheme() {
    let CSSStyle = document.head.querySelector("#theme-color");
    if (localStorage.getItem("Theme color")){
    CSSStyle.href = localStorage.getItem("Theme color");
    }
    changeThemeBtn.addEventListener("click", (event) => {
        let themeColor = CSSStyle.getAttribute("href");
        if (themeColor.includes("light-main.css")) {
            CSSStyle.href = "dark-main.css"
        } else if (themeColor.includes("dark-main.css")) {
            CSSStyle.href = "light-main.css"
        }

        localStorage.setItem("Theme color", CSSStyle.href);

    });
}

changeTheme();