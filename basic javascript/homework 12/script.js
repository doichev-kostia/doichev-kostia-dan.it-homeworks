const images = document.querySelectorAll(".image-to-show");
const stopOnCLickBtn = document.querySelector(".stop-btn");
const playOnClickBtn = document.querySelector(".play-btn");
let counter = 0;

function onButtonClick() {
    document.addEventListener("click", event => {
        if (event.target.classList === stopOnCLickBtn.classList) {
            clearTimeout(showImagesWithDelay);// || clearInterval(showImagesWithDelay);
        } else if (event.target.classList === playOnClickBtn.classList) {
            clearTimeout(showImagesWithDelay);// || clearInterval(showImagesWithDelay);
            showImages();
        }
    });

    let showImagesWithDelay;

    // function showImages() {
    //     showImagesWithDelay = setInterval(() => {
    //         counter++;
    //         if (counter !== 0) {
    //             images[counter - 1].hidden = true;
    //         }
    //
    //         if (counter / images.length === 1) {
    //             counter = 0;
    //         }
    //
    //         images[counter].hidden = false;
    //     }, 3000);
    // }
    function showImages() {
        showImagesWithDelay = setTimeout(function showImage()  {
            counter++;
            if (counter !== 0) {
                images[counter - 1].hidden = true;
            }

            if (counter / images.length === 1) {
                counter = 0;
            }

            images[counter].hidden = false;
            setTimeout(showImage,3000);
        }, 3000);
    }

    showImages()
}

onButtonClick();
